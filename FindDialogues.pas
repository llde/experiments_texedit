{
  New script template, only shows processed records
  Assigning any nonzero value to Result will terminate script
}
unit FindDialogues;

var
  count : integer;  
  // Called before processing
// You can remove it if script doesn't require initialization code
function Initialize: integer;
begin
  Result := 0;
end;

// called for every record selected in xEdit
function Process(e: IInterface): integer;
var
  quest_subel : IwbElement;
  quest_elem : IwbElement;
begin
  Result := 0;
  if ElementType(e) = etMainRecord then begin
    if Signature(e) <> 'INFO' then Exit;
	if ElementExists(e, 'QSTI') then begin
	  quest_subel := ElementBySignature(e, 'QSTI');
	  if Assigned(quest_subel) then begin
	    quest_elem := LinksTo(quest_subel);
        if EditorId(quest_elem) = 'MS22' then begin
		  AddMessage(Name(e) +' Text: ' + DisplayName(e));
        end;		
	  end;
	end;
  end;
end;

// Called after processing
// You can remove it if script doesn't require finalization code
function Finalize: integer;
begin
  Result := 0;
end;

end.