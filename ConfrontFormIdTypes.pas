{
  New script template, only shows processed records
  Assigning any nonzero value to Result will terminate script
}
unit ConfrontFormIDTypes;

var
ToFile: IInterface;
Masterf : string;
UOP : string;
Uberfile : lwbFile;
  
  // Called before processing
// You can remove it if script doesn't require initialization code
function Initialize: integer;
begin
  Result := 0;
  Masterf := 'Oblivion.esm';
  UOP := 'Unofficial Oblivion Patch.esp';  
  Uberfile := FileByLoadOrder(0);
  if not Assigned(Uberfile) then begin
    AddMessage('Cannot open Oblivion.esm');
  end;
end;

// called for every record selected in xEdit
function Process(e: IInterface): integer;
var
  record_base : IwbMainRecord;
  record_type : TwbElementType;
  sig : string;
  formid1 : integer;
begin
  Result := 0;
  record_type := ElementType(e);
  if record_type = etMainRecord then begin
    sig := Signature(e);
	formid1 := GetLoadOrderFormID(e);
	if Assigned(formid1) then begin 
	  record_base := RecordByFormId(Uberfile, formid1, false);
	 // if formid1 <> GetLoadOrderFormID(record_base) then Exit;
	  if Assigned(record_base) then begin
	    if sig <> Signature(record_base) then begin
          AddMessage(FullPath(e)+ ' has type ' + sig + ' should be ' + Signature(record_base));
	    end;
	  end
	  else if not Assigned(record_base) then AddMessage(FullPath(e) + ' RecordByFormId failed');
	end
	else if not Assigned(formid1) then begin 
	   AddMessage(FullPath(e) + ' not found on plugin');
	end  
  end;
end;

// Called after processing
// You can remove it if script doesn't require finalization code
function Finalize: integer;
begin
  Result := 0;
end;

end.