{
  New script template, only shows processed records
  Assigning any nonzero value to Result will terminate script
}
unit DetectNonOverrideRecords;

var
ToFile: IInterface;
Masterf : string;
UOP : string;
Uberfile : lwbFile;
  
  // Called before processing
// You can remove it if script doesn't require initialization code
function Initialize: integer;
begin
  Result := 0;
end;

// called for every record selected in xEdit
function Process(e: IInterface): integer;
var
  record_base : IwbMainRecord;
  record_type : TwbElementType;
  sig : string;
  formid1 : integer;
  formid2 : integer;
begin
  Result := 0;
  record_type := ElementType(e);
  if record_type = etMainRecord then begin
    sig := Signature(e);
	formid1 := GetLoadOrderFormID(e);
	if Assigned(formid1) then begin 
	  formid2 := formid1 shr 24;
	  if formid2 = GetLoadOrder(GetFile(e)) then begin
	     AddMessage(FullPath(e) + ' Not a override records');
	  end;
	end
	else if not Assigned(formid1) then begin 
	   AddMessage(FullPath(e) + ' not found on plugin');
	end  
  end;
end;

// Called after processing
// You can remove it if script doesn't require finalization code
function Finalize: integer;
begin
  Result := 0;
end;

end.